#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <stdio.h>
#include <unistd.h>
#include "DatosMemCompartida.h"


int main() {

        int fd_mmap;
        DatosMemCompartida *pdatos;
        struct stat sdatos;
        
        //Garantiza su ejecución el cuarto
        sleep(3);

	fd_mmap = open("/tmp/datos", O_RDWR);
        if (fd_mmap < 0) {
                perror("Error en la apertura del fichero");
                return 1;
        }

	fstat(fd_mmap, &sdatos);
	pdatos = (DatosMemCompartida*)mmap(NULL, sdatos.st_size, PROT_READ|PROT_WRITE, MAP_SHARED, fd_mmap, 0);	
        close(fd_mmap);


        while(1)
        {
        	usleep(25000); //en suspensión durante 25 ms
               if (pdatos->esfera.centro.y + pdatos->esfera.radio < pdatos->raqueta1.y1) 
                        pdatos->accion = -1;
               else if (pdatos->esfera.centro.y  - pdatos->esfera.radio > pdatos->raqueta1.y2) 
                        pdatos->accion = 1;
		else
			 pdatos->accion = 0;
        }


        munmap (pdatos, sdatos.st_size); //desproyección de memoria
        return 0;

}
