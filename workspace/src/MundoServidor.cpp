// MundoServidor.cpp: implementation of the CMundo class.
//
//////////////////////////////////////////////////////////////////////
#include <fstream>
#include "MundoServidor.h"
#include "glut.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>


#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/mman.h>

#include <signal.h>

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

//Funciones de thread

void* hilo_comandos(void* d){
	CMundoServidor* p = (CMundoServidor*) d;
	p->RecibeComandos();
	return p;
}


void CMundoServidor::RecibeComandos()
{
        while (1) {
                usleep(10);
                char cad[100];
                read(fd_lectura_ct, cad, sizeof(cad));
                unsigned char key;
                sscanf(cad,"%c",&key);
                
                
                if(key=='s')jugador1.velocidad.y=-4;
                if(key=='w')jugador1.velocidad.y=4;
                if(key=='l')jugador2.velocidad.y=-4;
                if(key=='o')jugador2.velocidad.y=4;
                
        }

}

//Señales

void funcion_signal(int n)
{
        char cmd[200];
        printf("\nSeñal %s capturada (%d)\n", strsignal(n), n);
        sprintf(cmd, "ps %d", getpid()); //estado del proceso
        system(cmd);
        unlink("/tmp/tuberia_ct"); //se eliminan las tuberías
        unlink("/tmp/tuberia_sc");
        sprintf(cmd, "pkill bot"); //se mata al proceso bot
        system(cmd);
        if(n == 12)
                exit(0);
        else
                exit(n);
}




CMundoServidor::CMundoServidor()
{
	Init();
}

CMundoServidor::~CMundoServidor()
{
	//El servidor espera para que el cliente cierre primero
	sleep(1);
	
	close(fd_escritura_sl); //cierre de la tubería servidor-logger
	close(fd_escritura_sc); //cierre de la tubería servidor-cliente
	close(fd_lectura_ct);   //cierre de la tubería cliente-thread
	//La desproyeccion de memoria se hace en CMundoCliente
}

void CMundoServidor::InitGL()
{
	//Garantizar su ejecución el tercero
	sleep(2);
	
	//Habilitamos las luces, la renderizacion y el color de los materiales
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);
	
	glMatrixMode(GL_PROJECTION);
	gluPerspective( 40.0, 800/600.0f, 0.1, 150);
}

void print(char *mensaje, int x, int y, float r, float g, float b)
{
	glDisable (GL_LIGHTING);

	glMatrixMode(GL_TEXTURE);
	glPushMatrix();
	glLoadIdentity();

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, glutGet(GLUT_WINDOW_WIDTH), 0, glutGet(GLUT_WINDOW_HEIGHT) );

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	glColor3f(r,g,b);
	glRasterPos3f(x, glutGet(GLUT_WINDOW_HEIGHT)-18-y, 0);
	int len = strlen (mensaje );
	for (int i = 0; i < len; i++) 
		glutBitmapCharacter (GLUT_BITMAP_HELVETICA_18, mensaje[i] );
		
	glMatrixMode(GL_TEXTURE);
	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glEnable( GL_DEPTH_TEST );
}
void CMundoServidor::OnDraw()
{
	//Borrado de la pantalla	
   	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Para definir el punto de vista
	glMatrixMode(GL_MODELVIEW);	
	glLoadIdentity();
	
	gluLookAt(0.0, 0, 17,  // posicion del ojo
		0.0, 0.0, 0.0,      // hacia que punto mira  (0,0,0) 
		0.0, 1.0, 0.0);      // definimos hacia arriba (eje Y)    

	/////////////////
	///////////
	//		AQUI EMPIEZA MI DIBUJO
	char cad[100];
	sprintf(cad,"Jugador1: %d",puntos1);
	print(cad,10,0,1,1,1);
	sprintf(cad,"Jugador2: %d",puntos2);
	print(cad,650,0,1,1,1);
	int i;
	for(i=0;i<paredes.size();i++)
		paredes[i].Dibuja();

	fondo_izq.Dibuja();
	fondo_dcho.Dibuja();
	jugador1.Dibuja();
	jugador2.Dibuja();
	esfera.Dibuja();

	/////////////////
	///////////
	//		AQUI TERMINA MI DIBUJO
	////////////////////////////

	//Al final, cambiar el buffer
	glutSwapBuffers();
}

void CMundoServidor::OnTimer(int value)
{	
	jugador1.Mueve(0.025f);
	jugador2.Mueve(0.025f);
	esfera.Mueve(0.025f);
	int i;
	for(i=0;i<paredes.size();i++)
	{
		paredes[i].Rebota(esfera);
		paredes[i].Rebota(jugador1);
		paredes[i].Rebota(jugador2);
	}

	jugador1.Rebota(esfera);
	jugador2.Rebota(esfera);
	if(fondo_izq.Rebota(esfera))
	{
		esfera.radio=0.5;
		time = 0;
		
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=2+2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=2+2*rand()/(float)RAND_MAX;
		puntos2++;
		
		marcador.puntos2 = puntos2;
		marcador.tanto=2;
		
		//se escribe la estructura en la tubería
		write(fd_escritura_sl, &marcador, sizeof (marcador)); 		
		
		if(puntos2 == 3)
		//el juego termina, hay que actualizar
		{
			char cad[200]; 	
			sprintf(cad,"%f %f %f %f %f %f %f %f %f %f %d %d", esfera.centro.x,esfera.centro.y, jugador1.x1,jugador1.y1,jugador1.x2,jugador1.y2,jugador2.x1,jugador2.y1,jugador2.x2,jugador2.y2, puntos1, puntos2);
			write(fd_escritura_sc, cad, sizeof(cad));
			exit(0);

			}
		
	}

	if(fondo_dcho.Rebota(esfera))
	{
		esfera.radio=0.5;
		time = 0;
		
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=-2-2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=-2-2*rand()/(float)RAND_MAX;
		puntos1++;
		
		marcador.puntos1 = puntos1;
		marcador.tanto=1;
		
		write(fd_escritura_sl, &marcador, sizeof (marcador));

		if(puntos1 == 3)
		{

			char cad[200];
			sprintf(cad,"%f %f %f %f %f %f %f %f %f %f %d %d", esfera.centro.x,esfera.centro.y, jugador1.x1,jugador1.y1,jugador1.x2,jugador1.y2,jugador2.x1,jugador2.y1,jugador2.x2,jugador2.y2, puntos1, puntos2);
			write(fd_escritura_sc, cad, sizeof(cad));
			exit(0);

		}
	}



	time ++;
	if (time == 80 && esfera.radio > 0.2f){ //Cada 2s
		esfera.radio -=0.05;
		time = 0;
		}
	

  	//Escritura en la tubería servidor-cliente
	char cad[200];
	sprintf(cad,"%f %f %f %f %f %f %f %f %f %f %d %d", esfera.centro.x,esfera.centro.y, jugador1.x1,jugador1.y1,jugador1.x2,jugador1.y2,jugador2.x1,jugador2.y1,jugador2.x2,jugador2.y2, puntos1, puntos2);
	write(fd_escritura_sc, cad, sizeof(cad));      
}

void CMundoServidor::OnKeyboardDown(unsigned char key, int x, int y)
{
/*
	switch(key)
	{
//	case 'a':jugador1.velocidad.x=-1;break;
//	case 'd':jugador1.velocidad.x=1;break;
	case 's':jugador1.velocidad.y=-4;break;
	case 'w':jugador1.velocidad.y=4;break;
	case 'l':jugador2.velocidad.y=-4;break;
	case 'o':jugador2.velocidad.y=4;break;

	}
*/
}

void CMundoServidor::Init()
{
	Plano p;
//pared inferior
	p.x1=-7;p.y1=-5;
	p.x2=7;p.y2=-5;
	paredes.push_back(p);

//superior
	p.x1=-7;p.y1=5;
	p.x2=7;p.y2=5;
	paredes.push_back(p);

	fondo_izq.r=0;
	fondo_izq.x1=-7;fondo_izq.y1=-5;
	fondo_izq.x2=-7;fondo_izq.y2=5;

	fondo_dcho.r=0;
	fondo_dcho.x1=7;fondo_dcho.y1=-5;
	fondo_dcho.x2=7;fondo_dcho.y2=5;

	//a la izq
	jugador1.g=0;
	jugador1.x1=-6;jugador1.y1=-1;
	jugador1.x2=-6;jugador1.y2=1;

	//a la dcha
	jugador2.g=0;
	jugador2.x1=6;jugador2.y1=-1;
	jugador2.x2=6;jugador2.y2=1;
	
	
	time = 0;
	

	//Escritura de la tuberia SERVIDOR-LOGGER
        fd_escritura_sl = open ("/tmp/tuberia_sl", O_WRONLY | O_CREAT);
        if (fd_escritura_sl < 0)
        {
                perror ("Error. No se puede abrir para escritura la tubería servidor-logger");
                unlink("/tmp/tuberia_sl");
        }


        //Escritura de la tuberia SERVIDOR-CLIENTE
        fd_escritura_sc = open ("/tmp/tuberia_sc", O_WRONLY | O_CREAT);
        if(fd_escritura_sc < 0)
                perror("Error. No se puede abrir para escritura la tubería servidor-cliente");

	//Dejar tiempo para que el cliente cree la tuberia cliente-thread
	sleep(1); 

        //Lectura de la tuberia CLIENTE-THREAD
        fd_lectura_ct = open ("/tmp/tuberia_ct", O_RDONLY);
        if (fd_lectura_ct < 0)
                perror("Error. No se ha puede abrir para lectura la tubería cliente-thread");

	//Creacion del THREAD
        pthread_create(&thread, NULL, hilo_comandos, this);
	
	
	//Señales
	struct sigaction act;
        act.sa_handler = &funcion_signal;
        sigaction(SIGINT, &act, NULL);
        sigaction(SIGTERM, &act, NULL);
        sigaction(SIGPIPE, &act, NULL);
        sigaction(SIGUSR2, &act, NULL);
}
