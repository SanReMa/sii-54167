// MundoCliente.cpp: implementation of the CMundo class.
//
//////////////////////////////////////////////////////////////////////
#include <fstream>
#include "MundoCliente.h"
#include "glut.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>


#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/mman.h>

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////



CMundoCliente::CMundoCliente()
{
	Init();
}

CMundoCliente::~CMundoCliente()
{
	//cierre de la tubería servidor-cliente
	close(fd_lectura_sc); 
	unlink("/tmp/tuberia_sc");
	//cierre de la tubería cliente-thread
	close(fd_escritura_ct); 
	unlink("/tmp/tuberia_ct");


	munmap(pdatos, mem_stat.st_size); //desproyección de memoria
}

void CMundoCliente::InitGL()
{
	//Habilitamos las luces, la renderizacion y el color de los materiales
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);
	
	glMatrixMode(GL_PROJECTION);
	gluPerspective( 40.0, 800/600.0f, 0.1, 150);
}

void print(char *mensaje, int x, int y, float r, float g, float b)
{
	glDisable (GL_LIGHTING);

	glMatrixMode(GL_TEXTURE);
	glPushMatrix();
	glLoadIdentity();

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, glutGet(GLUT_WINDOW_WIDTH), 0, glutGet(GLUT_WINDOW_HEIGHT) );

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	glColor3f(r,g,b);
	glRasterPos3f(x, glutGet(GLUT_WINDOW_HEIGHT)-18-y, 0);
	int len = strlen (mensaje );
	for (int i = 0; i < len; i++) 
		glutBitmapCharacter (GLUT_BITMAP_HELVETICA_18, mensaje[i] );
		
	glMatrixMode(GL_TEXTURE);
	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glEnable( GL_DEPTH_TEST );
}
void CMundoCliente::OnDraw()
{
	//Borrado de la pantalla	
   	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Para definir el punto de vista
	glMatrixMode(GL_MODELVIEW);	
	glLoadIdentity();
	
	gluLookAt(0.0, 0, 17,  // posicion del ojo
		0.0, 0.0, 0.0,      // hacia que punto mira  (0,0,0) 
		0.0, 1.0, 0.0);      // definimos hacia arriba (eje Y)    

	/////////////////
	///////////
	//		AQUI EMPIEZA MI DIBUJO
	char cad[100];
	sprintf(cad,"Jugador1: %d",puntos1);
	print(cad,10,0,1,1,1);
	sprintf(cad,"Jugador2: %d",puntos2);
	print(cad,650,0,1,1,1);
	int i;
	for(i=0;i<paredes.size();i++)
		paredes[i].Dibuja();

	fondo_izq.Dibuja();
	fondo_dcho.Dibuja();
	jugador1.Dibuja();
	jugador2.Dibuja();
	esfera.Dibuja();

	/////////////////
	///////////
	//		AQUI TERMINA MI DIBUJO
	////////////////////////////

	//Al final, cambiar el buffer
	glutSwapBuffers();
}

void CMundoCliente::OnTimer(int value)
{	
        //LECTURA DE LA TUBERÍA SERVIDOR CLIENTE

	char cad[200];
	if(read(fd_lectura_sc, cad, sizeof(cad)) >1 )
	sscanf(cad,"%f %f %f %f %f %f %f %f %f %f %d %d", &esfera.centro.x,&esfera.centro.y, &jugador1.x1,&jugador1.y1,&jugador1.x2,&jugador1.y2, &jugador2.x1,&jugador2.y1,&jugador2.x2,&jugador2.y2, &puntos1, &puntos2);	
        
        //Salida del programa si uno de los jugadores anota 3 puntos
        if(puntos1 ==  3 || puntos2 == 3)
        	exit(0);




	jugador1.Mueve(0.025f);
	jugador2.Mueve(0.025f);
	esfera.Mueve(0.025f);
	int i;
	for(i=0;i<paredes.size();i++)
	{
		paredes[i].Rebota(esfera);
		paredes[i].Rebota(jugador1);
		paredes[i].Rebota(jugador2);
	}

	jugador1.Rebota(esfera);
	jugador2.Rebota(esfera);
	if(fondo_izq.Rebota(esfera))
	{
		esfera.radio=0.5;
		time = 0;
		
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=2+2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=2+2*rand()/(float)RAND_MAX;

	}

	if(fondo_dcho.Rebota(esfera))
	{
		esfera.radio=0.5;
		time = 0;
		
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=-2-2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=-2-2*rand()/(float)RAND_MAX;

	}



	time ++;
	if (time == 80 && esfera.radio > 0.2f){ //Cada 2s
		esfera.radio -=0.05;
		time = 0;
		}
	
	//Lectura de memoria compartida
	pdatos->esfera = esfera;
	pdatos->raqueta1 = jugador1;
	
	if (pdatos->accion == 1) //la raqueta sube
                OnKeyboardDown ('w', 0, 0);
        if (pdatos->accion == -1) //la raqueta baja
                OnKeyboardDown ('s', 0, 0);
        if (pdatos->accion == 0) {} 
        

}

void CMundoCliente::OnKeyboardDown(unsigned char key, int x, int y)
{
	char cad[200];
	sprintf(cad, "%c", key);
	write(fd_escritura_ct, &cad, strlen(cad));
/*
	switch(key)
	{
//	case 'a':jugador1.velocidad.x=-1;break;
//	case 'd':jugador1.velocidad.x=1;break;
	case 's':jugador1.velocidad.y=-4;break;
	case 'w':jugador1.velocidad.y=4;break;
	case 'l':jugador2.velocidad.y=-4;break;
	case 'o':jugador2.velocidad.y=4;break;

	}
*/
write(fd_escritura_ct, &key, sizeof(key));
}

void CMundoCliente::Init()
{
	//Garantizar su ejecución el segundo
	sleep(1);
	
	
	Plano p;
//pared inferior
	p.x1=-7;p.y1=-5;
	p.x2=7;p.y2=-5;
	paredes.push_back(p);

//superior
	p.x1=-7;p.y1=5;
	p.x2=7;p.y2=5;
	paredes.push_back(p);

	fondo_izq.r=0;
	fondo_izq.x1=-7;fondo_izq.y1=-5;
	fondo_izq.x2=-7;fondo_izq.y2=5;

	fondo_dcho.r=0;
	fondo_dcho.x1=7;fondo_dcho.y1=-5;
	fondo_dcho.x2=7;fondo_dcho.y2=5;

	//a la izq
	jugador1.g=0;
	jugador1.x1=-6;jugador1.y1=-1;
	jugador1.x2=-6;jugador1.y2=1;

	//a la dcha
	jugador2.g=0;
	jugador2.x1=6;jugador2.y1=-1;
	jugador2.x2=6;jugador2.y2=1;
	
	
	time = 0;
	
	//Tuberia Cliente-Servidor
        int tuberia_sc;
        tuberia_sc = mkfifo("/tmp/tuberia_sc", 0666);
        if(tuberia_sc < 0)
                perror("Error. No se puede crear la tubería servidor-cliente");
        else
                printf("Tubería servidor-cliente creada correctamente\n");

        fd_lectura_sc = open("/tmp/tuberia_sc", O_RDONLY);
        if(fd_lectura_sc < 0)
                perror("No se puede abrir para lectura la tubería servidor-cliente");



        //Tuberia Cliente-Thread
        int tuberia_ct;
        tuberia_ct = mkfifo("/tmp/tuberia_ct", 0666);
        if(tuberia_ct < 0)
                perror("No se puede crear la tubería cliente-thread");
        else
                printf("Tubería cliente-thread creada correctamente\n");


        fd_escritura_ct = open("/tmp/tuberia_ct", O_WRONLY | O_CREAT);
        if(fd_escritura_ct < 0)
                perror("No se puede abrir para escritura la tubería cliente-thread");

 

	//Proyeccion en memoria
	fd_mmap = open ("/tmp/datos", O_CREAT|O_TRUNC|O_RDWR, 0666);
        if (fd_mmap  < 0)
        {
                perror("Error. No se puede abrir el fichero de datos");
        }

        write (fd_mmap, &datos, sizeof(datos));

	fstat(fd_mmap, &mem_stat);
	pdatos=(DatosMemCompartida*)mmap(NULL, mem_stat.st_size, PROT_READ|PROT_WRITE, MAP_SHARED, fd_mmap, 0);
        close(fd_mmap);

}
