#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>

int main() {

        int fd_lectura_sl, tuberia_sl;
        typedef struct {
                int puntos1; //Puntos del jugador1
                int puntos2; //Puntos del jugador2
                int tanto;   //Ultimo jugador en marcar (1=jugador1, 2=jugador2)
        } Marcador;
        Marcador marcador;

	tuberia_sl = mkfifo("/tmp/tuberia_sl", 0666); 
        if ( tuberia_sl < 0) //Crear tubería 
        {
                perror("Error al crear la tuberia servidor-logger");
                return 1;
        }
        else
        	printf("Tuberia servidor-logger creada correctamente\n");


	

	fd_lectura_sl = open("/tmp/tuberia_sl", O_RDONLY);
        if (fd_lectura_sl < 0) //Abrir tubería para lectura
        {
                perror("Error al abrir la tuberia servidor-logger para lectura");
                return 1;
        }



        while (read (fd_lectura_sl, &marcador, sizeof(marcador)) == sizeof(marcador)) //lee mientras el tamaño de lo leído sea el tamaño de la estructura
        {
                if (marcador.tanto == 1){
                        printf ("Jugador 1 marca 1 punto, lleva un total de %d puntos.\n", marcador.puntos1);
                        }
                else if (marcador.tanto == 2){
                        printf ("Jugador 2 marca 1 punto, lleva un total de %d puntos.\n", marcador.puntos2);
                        }
		
		
		if (marcador.puntos1 ==3){
			printf("Jugador 1 gana. Fin de la partida.\n");
			}
		else if (marcador.puntos2 ==3){
			printf("Jugador 2 gana. Fin de la partida.\n");
			}
			
        }


        close(fd_lectura_sl); //Cerrar extremo de lectura de la tubería
        unlink("/tmp/tuberia_sl"); //Eliminar tubería
        return 0;

}
