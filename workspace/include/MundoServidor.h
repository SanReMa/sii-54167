// Autor: Sandra Rebollo Malvar

// MundoServidor.h: interface for the CMundo class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_MUNDOSERVIDOR_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_)
#define AFX_MUNDOSERVIDOR_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_

#include <vector>
#include "Plano.h"

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

//#include "Esfera.h"
//#include "Raqueta.h"
#include "DatosMemCompartida.h"

#include <sys/stat.h>

#include <pthread.h>

class CMundoServidor 
{
public:
	void Init();
	CMundoServidor();
	virtual ~CMundoServidor();	
	
	void InitGL();	
	void OnKeyboardDown(unsigned char key, int x, int y);
	void OnTimer(int value);
	void OnDraw();	

	Esfera esfera;
	std::vector<Plano> paredes;
	Plano fondo_izq;
	Plano fondo_dcho;
	Raqueta jugador1;
	Raqueta jugador2;

	int puntos1;
	int puntos2;
	
	int time; //Para disminuir el tamaño de la esfera cada x tiempo
	
	int fd_escritura_sc;  //Escritura de la tuberia servidor-cliente
	int fd_escritura_sl;  //Escritura de la tuberia servidor-logger
	int fd_lectura_ct;//Lectura de la tuberia thread-cliente
	
	pthread_t thread;
	//función para el thread
	void RecibeComandos(); 
	
	//Estructura para la puntuación
       typedef struct {
                int puntos1; //Puntos del jugador1
                int puntos2; //Puntos del jugador2
                int tanto;   //Ultimo jugador en marcar (1=jugador1, 2=jugador2)
        } Marcador;
        Marcador marcador;
	
	DatosMemCompartida datos;
	DatosMemCompartida* pdatos;
	
	struct stat mem_stat;
	int fd_mmap;
};

#endif // !defined(AFX_MUNDOCLIENTE_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_)
