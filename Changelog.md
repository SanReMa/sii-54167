# Change Log

## [v1.4] - 08-12-2021

### Added
	-Creación de un Cliente y un Servidor
	-Creación de la comunicación entre cliente y servidor
	-Creación de la rama Practica 4
	
### Modified
	-Añadido sleeps para garantizar el orden de ejecución
	-Añadido la captura de señales


## [v1.3] - 23-11-2021

### Added
	-Creación del Logger
	-Creación del Bot
	-Creación de la rama Practica 3

### Modified
	-Añadido funcionalidad para finalizar el programa a los 3 puntos
	
	
## [v1.2] - 03-11-2021

### Added
	-Creación del README
	-Creación de la rama Practica 2

### Modified
	-Autor añadido a la cabecera de los ficheros
	-Añadido movimiento a las esferas
	-Añadido movimiento a las raquetas
	-Añadido método de redución del tamaño de las esferas


## [v1.1] - 08-10-2021

### Added
	-Creación del Changelog


